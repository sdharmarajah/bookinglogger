import constants
import settings
from models.StandardResponse import StandardResponse
from models.MongoBooking import MongoBooking
from mongoengine import *
# connect to mongo
settings.connect_to_mongo()
import threading
from services.APIManagerService import APIManagerService

class BookingService(object):

    @staticmethod
    def upsert(json):
        # update/insert this record to Mongo
        try:
            # extract appointment ID and match
            incoming_booking = MongoBooking.init_from_json(booking_json=json)

            # before interacting with the DB make an async POST to the oDoc AI server
            # BookingService.send_booking_notification(booking=incoming_booking)
            thread = threading.Thread(target=BookingService.send_booking_notification, args=(), kwargs={constants.kBooking: incoming_booking})
            thread.start()

            matched_booking = BookingService.get_booking_for_appointment_id(incoming_booking.appointment_id)

            if matched_booking is None:
                print("No matching booking found: insert")
                BookingService.insert(booking=incoming_booking)
            else:
                print("Matching booking found: update")
                BookingService.update(matched_booking=matched_booking, incoming_booking=incoming_booking)

        except Exception as e:
            raise Exception("Something went wrong when upserting: " + type(e).__name__ + " " + str(e))

    @staticmethod
    def update(matched_booking, incoming_booking):
        # update last event of the matched booking and save booking
        matched_booking.last_event = incoming_booking.last_event
        matched_booking.save()

    @staticmethod
    def insert(booking):
        # save to Mongo
        booking.save()

    @staticmethod
    def get_booking_for_appointment_id(appointment_id):
        booking = MongoBooking.objects(appointment_id=appointment_id).first()
        return booking

    @staticmethod
    def get_open_bookings_in_range(start, end, session_type):
        bookings = [booking.to_dict() for booking in MongoBooking.objects(Q(estimated_start_time__gte=start) & Q(estimated_start_time__lt=end) & Q(session_type=session_type) & Q(is_open=True))]
        standard_response = StandardResponse(success=True, response=bookings)
        return standard_response

    @staticmethod
    def send_booking_notification(booking):
        APIManagerService.send_booking_notification(booking)
