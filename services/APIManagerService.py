import settings
import requests
from config import kIsDevelopment
import constants
import json


class APIManagerService(object):

    # POST a booking notification to the oDoc AI server
    @staticmethod
    def send_booking_notification(booking):
        endpoint = settings.get_odocai_endpoint()
        headers = {
            'Authorization': settings.authorization_token(),
            'Accept': "application/json",
            'Content-Type': "application/json"
        }

        # TODO: Fix: not returning the response here
        if not kIsDevelopment:
            requests.post(url=endpoint, data=json.dumps({constants.kBooking: booking.to_dict()}), headers=headers)

    # dummy testing method
    @staticmethod
    def dummy():
        endpoint = settings.get_odocai_endpoint()
        headers = {
            'Authorization': settings.authorization_token(),
            'Accept': "application/json",
            'Content-Type': "application/json"
        }

        # TODO: Fix: not returning the response here
        if not kIsDevelopment:
            requests.post(url=endpoint, data=json.dumps({"test": "test"}), headers=headers)
