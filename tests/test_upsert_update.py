import sys
sys.path.append('../')
from services.BookingService import BookingService

json = {
    "event": "cancelledConsultation",
    "data": {
        "_id": "84226d80-b639-11e8-b671-03f326980dbb",
        "_version": 11,
        "bookingId": "8220d120-b639-11e8-ba42-cb336de6d9f0",
        "appointmentId": "80087870-b639-11e8-8b8e-b5e679148547",
        "sessionId": "a5813fb0-b512-11e8-8b8e-b5e679148547",
        "tokenNumber": 1,
        "gen1Id": "16094",
        "patient": {
            "_version": 3,
            "patientId": "pa_2",
            "firstName": "Sohan",
            "lastName": "Dharmarajah",
            "gender": "M",
            "dateOfBirth": 1502323200000,
            "phoneNumber": "94777660664",
            "customized": False
        },
        "doctor": {
            "_version": 4,
            "doctorId": "dr_3",
            "firstName": "Demo",
            "lastName": "Consultant 1",
            "type": "doctor",
            "speciality": "General Practitioner",
            "title": "Dr.",
            "registrationNo": "123",
            "country": "Sri Lanka",
            "profilePictureUrl": "https://api.odoc.life/oDocApiV8/Uploads/ProPic/anilf.jpg"
        },
        "creator": {
            "_version": 3,
            "type": "patient",
            "id": "pa_2",
            "fullName": "Sohan Dharmarajah",
            "isPublicAccount": False
        },
        "bookedTime": 1536721874738,
        "callEvents": [],
        "status": "Pending",
        "isAnonymous": False,
        "doctorCharged": False,
        "type": "virtual",
        "chat": {
            "_version": 2,
            "threadId": "7c6b4f91-045d-4354-b520-33542adfc7af",
            "sessionId": "-LMAkjmujLEy_vOEglnb",
            "consultationId": "84226d80-b639-11e8-b671-03f326980dbb",
            "status": "active",
            "disableTime": 1536765298187
        }
    }
}

BookingService.upsert(json=json)
