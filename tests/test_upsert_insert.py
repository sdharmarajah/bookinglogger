import sys
sys.path.append('../')
from services.BookingService import BookingService

json = {
   "event": "newConsultation",
   "data": {
      "_id": "bf63b1c0-dd89-11e8-811a-0b4c9073531d",
      "_version": 11,
      "bookingId": "bef9f370-dd89-11e8-a71a-df77e4771617",
      "appointmentId": "bd8ef1c0-dd89-11e8-a7d7-c9cede5d156b",
      "tokenNumber": 3,
      "gen1Id": "119164",
      "patient": {
         "_version": 4,
         "patientId": "pa_9915766",
         "firstName": "Test",
         "lastName": "Test",
         "phoneNumber": "94777346798",
         "customized": False
      },
      "doctor": {
         "_version": 4,
         "doctorId": "dr_3",
         "firstName": "Demo",
         "lastName": "Consultant 1",
         "type": "doctor",
         "speciality": "General Practitioner ",
         "title": "Dr.",
         "registrationNo": "123458",
         "country": "Sri Lanka",
         "profilePictureUrl": "https://api.odoc.life/oDocApiV9/Uploads/ProPic/anilf.jpg"
      },
      "creator": {
         "_version": 3,
         "type": "patient",
         "id": "pa_9915766",
         "fullName": "Test Test",
         "isPublicAccount": False
      },
      "bookedTime": 1541044432606,
      "platform": "odoc",
      "callEvents": [

      ],
      "status": "Pending",
      "isAnonymous": False,
      "doctorCharged": False,
      "type": "virtual",
      "sessionType": "select",
      "appointmentDate": 1541082600000,
      "chat": {
         "_version": 2,
         "threadId": "201f1e37-4c57-4971-8bc4-5ad5111b7cdb",
         "sessionId": "-LQCOyxL8UV_Vc1jx1Ne",
         "consultationId": "bf63b1c0-dd89-11e8-811a-0b4c9073531d",
         "status": "active"
      }
   }
}

BookingService.upsert(json=json)
