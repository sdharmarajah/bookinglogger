import sys
sys.path.append('../')
from services.BookingService import BookingService
from pprint import pprint

start = 1537799400000
end = 1537799400001

standard_response = BookingService.get_open_bookings_in_range(start=start, end=end, session_type='select')
open_bookings = standard_response.response
print("Open SELECT bookings in range: " + str(start) + " <= t < " + str(end) + " are:")
for booking in open_bookings:
    pprint(booking)
