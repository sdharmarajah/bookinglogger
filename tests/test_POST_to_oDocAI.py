import sys
sys.path.append('../')
import settings
import requests
from config import kIsDevelopment
import json


endpoint = settings.get_odocai_endpoint()
headers = {
    'Authorization': settings.authorization_token(),
    'Accept': "application/json",
    'Content-Type': "application/json"
}

# TODO: Fix: not returning the response here
if not kIsDevelopment:
    requests.post(url=endpoint, data=json.dumps({"test": "test"}), headers=headers)
