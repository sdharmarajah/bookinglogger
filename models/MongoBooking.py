from mongoengine import *
import datetime
import constants
import json


# To log API calls
class MongoBooking(Document):
    last_event = StringField(required=True, max_length=30)
    start_time = LongField(required=False)  # not required (not present in booking updates)
    is_open = BooleanField(required=True)
    session_type = StringField(required=False, max_length=15)  # not required (not present in booking updates)
    session_id = StringField(required=True, max_length=50)
    appointment_id = StringField(required=True, max_length=50)
    estimated_start_time = LongField(required=False)  # not required (not present in booking updates)
    doctor_id = StringField(required=True, max_length=10)
    doctor_first_name = StringField(required=True, max_length=30)
    doctor_last_name = StringField(required=True, max_length=30)
    doctor_profile_picture_url = StringField(required=True, max_length=70)
    doctor_title = StringField(required=True, max_length=30)
    patient_id = StringField(required=True, max_length=10)
    platform = StringField(required=True, max_length=10)
    token_number = IntField(required=True, max_length=2)
    timestamp = DateTimeField(default=datetime.datetime.now)

    def save(self, *args, **kwargs):
        # update the timestamp
        self.timestamp = datetime.datetime.now()

        # update the open state based on the the last event
        self.update_is_open()
        return super(MongoBooking, self).save(*args, **kwargs)

    # set is_open to True if the booking's current value of "last_event" is 'newConsultation', False otherwise
    def update_is_open(self):
        if self.last_event == constants.kEventNewConsultation:
            self.is_open = True
        elif self.last_event == constants.kEventCancelledConsultation or self.last_event == constants.kEventCompletedConsultation:
            self.is_open = False

    # note: this static method DOES NOT SAVE the returned booking
    @staticmethod
    def init_from_json(booking_json):
        booking = MongoBooking()

        # extract top-level data
        booking.last_event = booking_json[constants.kEvent]

        # extract appointment ID
        data = booking_json[constants.kData]
        booking.appointment_id = data[constants.kAppointmentID]

        # extract data under appointment if it exists
        if constants.kAppointment in data and data[constants.kAppointment]:
            appointment = data[constants.kAppointment]
            booking.estimated_start_time = appointment[constants.kInitEstimate]

        # extract data under doctor
        doctor = data[constants.kDoctor]
        booking.doctor_id = doctor[constants.kDoctorId]
        booking.doctor_first_name = doctor[constants.kDoctorFirstName]
        booking.doctor_last_name = doctor[constants.kDoctorLastName]
        booking.doctor_title = doctor[constants.kDoctorTitle]
        booking.doctor_profile_picture_url = doctor[constants.kDoctorProfilePictureURL]

        # extract data under patient
        patient = data[constants.kPatient]
        booking.patient_id = patient[constants.kPatientID]

        # extract platform, token number and type
        booking.platform = data[constants.kPlatform]
        booking.token_number = data[constants.kTokenNumber]
        booking.session_type = data[constants.kType]

        # temporarily setting unavailable fields to dummy (or best available) values
        booking.start_time = data[constants.kBookedTime]
        booking.session_id = "not set"

        return booking

    def to_dict(self):
        return{
            constants.kEvent: self.last_event,
            constants.kBookingIsOpen: self.is_open,
            constants.kStartTime: self.start_time,
            constants.kPlatform: self.platform,
            constants.kTokenNumber: self.token_number,
            constants.kSessionType: self.session_type,
            constants.kSessionId: self.session_id,
            constants.kAppointmentID: self.appointment_id,
            constants.kAppointmentEstStartTime: self.estimated_start_time,
            constants.kDoctorId: self.doctor_id,
            constants.kPatientID: self.patient_id,
            constants.kDoctorTitle: self.doctor_title,
            constants.kDoctorFirstName: self.doctor_first_name,
            constants.kDoctorLastName: self.doctor_last_name,
            constants.kDoctorProfilePictureURL: self.doctor_profile_picture_url
        }

    def to_json(self):
        return json.dumps(self.to_dict())
