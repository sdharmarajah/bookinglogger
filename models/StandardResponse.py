from flask import make_response
import json

class StandardResponse(object):
    def __init__(self, success, error_message=None, response=None):
        self.success = success
        self.error_message = error_message
        self.response = response

    def to_dict(self):
        return {
            "success": self.success,
            "error_message": self.error_message,
            "result": self.response
        }

    def flask_response(self, response_code=200):
        return make_response(json.dumps(self.to_dict()), response_code)
