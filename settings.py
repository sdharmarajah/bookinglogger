from mongoengine import *
from config import kIsDevelopment

# Flask settings
flask_port = 9000


# connect to mongoDB depending on deployment location
def connect_to_mongo():
    if kIsDevelopment:
        connect('odoc-bookings')
    else:
        connect('odoc-bookings', username='odoc-booking-admin', password='odocbooking-a7adv6g91')


def get_odocai_endpoint():
    if kIsDevelopment:
        return "http://localhost:8005/bookings"
        # return "https://ai.odoc.life/bookings"
    else:
        return "https://ai.odoc.life/bookings"


def authorization_token():
    return "Token 99e7079a8d08ffc81373951aff78844d,universal"
