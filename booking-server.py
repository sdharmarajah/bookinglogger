#!/usr/bin/env python
from flask import Flask, g
from flask import request
from flask import make_response
from services.BookingService import BookingService
import json
import os
import settings
import constants
from models.StandardResponse import StandardResponse
from services.APIManagerService import APIManagerService

# connect to mongo
settings.connect_to_mongo()

# Flask app should start in global layout
app = Flask(__name__)

# detailed debugging of requests
@app.before_request
def log_request_info():
    app.logger.debug('Headers: %s', request.headers)
    app.logger.debug('Body: %s', request.get_data())


@app.before_request
@app.route('/booking', methods=['POST'])
def booking_api():
    if request.method == 'POST':

        try:
            payload = json.loads(request.data)
            standard_response = StandardResponse(success=True, response="OK")
            BookingService.upsert(payload)
            response = standard_response.flask_response()
        except Exception as e:
            standard_response = StandardResponse(success=False, response=str(e))
            response = standard_response.flask_response()
    else:
        error_message = "Something other than POST..."
        response = make_response(error_message, 500)
    return response


# get-open-bookings
# returns open bookings for start <= booking.estimated_start_time < end and session_type = "select"/"now"
# note: estimated_start_time is the time the appointment is supposed to begin (see next line)
# NOT the session start time
@app.route('/get-open-bookings', methods=['GET'])
def get_open_bookings_api():
    if request.method == 'GET':
        start = request.args.get(constants.kRangeStartTime)
        end = request.args.get(constants.kRangeEndTime)
        session_type = request.args.get(constants.kSessionType)
        if start is not None and end is not None and session_type is not None:
            standard_response = BookingService.get_open_bookings_in_range(start=start, end=end, session_type=session_type)
            response = standard_response.flask_response()
        else:
            error_message = constants.kRangeStartTime + ", " + constants.kRangeEndTime + " and/or " + constants.kSessionType + " was not passed or null."
            standard_response = StandardResponse(success=False, error_message=error_message)
            response = standard_response.flask_response()
    else:
        error_message = "Something other than GET..."
        response = make_response(error_message, 500)
    return response



if __name__ == '__main__':
    port = int(os.getenv('PORT', settings.flask_port))
    print("Starting app on port %d" % port)
    app.run(debug=True, port=port, host='0.0.0.0')
