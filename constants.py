kData = 'data'
kBooking = 'booking'
kAppointmentID = 'appointmentId'
kEvent = 'event'
kSession = 'session'
kSessionId = 'sessionId'
kId = 'id'
kStartTime = 'startTime'
kType = 'type'
kDoctor = 'doctor'
kDoctorId = 'doctorId'
kDoctorFirstName = 'firstName'
kDoctorLastName = 'lastName'
kDoctorTitle = 'title'
kDoctorProfilePictureURL = 'profilePictureUrl'
kPatient = 'patient'
kPatientID = 'patientId'
kAppointment = 'appointment'
kInitEstimate = 'initEstimate'
kAppointmentEstStartTime = 'estimated_start_time'
kPlatform = 'platform'
kTokenNumber = 'tokenNumber'
kBookedTime = 'bookedTime'

# time range constants
kRangeStartTime = 'start'
kRangeEndTime = 'end'

# booking type constants
kSessionType = 'type'

# Booking events
kEventNewConsultation = "newConsultation"
kEventCancelledConsultation = "cancelledConsultation"
kEventDocSawPatientInQueue = "doctorSeenPatientInQueue"
kEventCompletedConsultation = "completedConsultation"

# Booking parameters
kBookingIsOpen = 'is_open'
